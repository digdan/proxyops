var http = require('http'),  
httpProxy = require('http-proxy'),
url = require('url'),
proxy = httpProxy.createProxyServer({});
proxyStats = require('../proxystats/index.js');

http.createServer(function(req, res) {
	var hostname = req.headers.host.split(":")[0];
	var pathname = url.parse(req.url).pathname;
	if(pathname.indexOf("/geo/") === 0) {
		proxy.web(req, res, {target:"http://127.0.0.1:8081"});
	} else if (pathname.indexOf("/proxy/") === 0) {
		var last = pathname.substring(pathname.lastIndexOf("/") + 1, pathname.length);
		var proxyParts = last.split(":");
		console.log(proxyParts);
		proxyStats.testProxy(proxyParts[0],proxyParts[1],function(results) {
			res.end(JSON.stringify(results));
		});
	} else if (pathname.indexOf("/mirror/") === 0) {
		var cache = [];
		var mirror = JSON.stringify(req.headers,function(key,value) {
			if (typeof value === 'object' && value !== null) {
				if (cache.indexOf(value) !== -1) return;
				cache.push(value);
			}
			return value;
		});
		cache=null;
		res.end(mirror);
	} else {
		res.end('No endpoint found.');
	}
}).listen(8080);
