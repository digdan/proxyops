var geoip=require('geoip-native');
var restify = require('restify');

var server = restify.createServer({
  name: 'ProxyOps Geoip',
  version: '1.0.0'
});
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

server.get('/geo/:ip', function (req, res, next) {
	var loc = geoip.lookup(req.params.ip);
	res.send(JSON.stringify(loc));
	return next();
});

server.listen(8081, function () {
  console.log('%s listening at %s', server.name, server.url);
});
