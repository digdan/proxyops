var http=require('http');
var unirest=require('unirest');
var ipMark='104.236.150.52';
function testProxy(ip,port,callback) {
	var code=0;
	var message;
	var start,connect,diff;
	var options = {
		host:ip,
		port:port,
		path:"http://api.proxyops.com/mirror/",
		headers:{
			'Host':'api.proxyops.com',
			'User-Agent':'ProxyOps Mirror (http://www.proxyops.com)'
		},
		method:"GET"
	};
	start = process.hrtime();
	var req = http.request(options, function(res) {
		connect = process.hrtime(start);			
		code=res.statusCode;
		res.setEncoding('utf8');
		res.on('data',function(chunk) {
			var decoded;
			try {
				decoded = JSON.parse(chunk);
			} catch (e) {
				callback({
					code:code,
					error:e,
					data:chunk
				});
				req.abort();
				return;
			}
			//Now compare the two
			if (chunk.indexOf(ipMark) > -1) {
				var ipHidden=false;
			} else {
				var ipHidden=true;
			}
			if ((chunk.indexOf( 'Forward') > -1)
				 || (chunk.indexOf( 'forward') > -1) 
				 || (chunk.indexOf( 'Via') > -1) 
				 || (chunk.indexOf( 'via') > -1) 
				 || (chunk.indexOf( 'proxy') > -1) 
				 || (chunk.indexOf( 'Proxy') > -1) 
				){
				var proxyHidden=false;
			} else {
				var proxyHidden=true;
			}

			//Grab location from geoip api service
			unirest.get('http://127.0.0.1:8081/geo/'+ip)
			.end(function (response) {
				var geodecoded,geo;
				try {
					geodecoded = JSON.parse(response.body);
					geo = geodecoded;
				} catch(e) {
					geo = 'Unknown';
				}
				diff = process.hrtime(start);
				callback({
					target:ip+':'+port,
					code:code,
					geo:geo,
					duration:{
						connect:((connect[0] * 1e9 + connect[1])/1000000000),
						total:((diff[0] * 1e9 + diff[1])/1000000000)
					},
					ipHidden:ipHidden,
					proxyHidden:proxyHidden,
					recorded:(Math.floor(new Date() / 1000))
				});
				req.abort();
			});
		});
	});
	req.on('error',function(err) {
		callback({
			code:-1,
			error:err.message
		})
		req.abort();
	});
	req.end();
};
module.exports = {
	testProxy:testProxy
}
